Assessment = {};

Assessment.mergeSort = function(array) {};

Assessment.recursiveExponent = function(base, power) {
  if (power === 1){
    return base;
  };
  return base * Assessment.recursiveExponent(base, power - 1)
};

Assessment.powCall = function (base, power, callback) {
  var result = Assessment.recursiveExponent(base, power);
  callback(result);
  return result;

};

Assessment.transpose = function (matrix) {
  var result = [];
  for (var k = 0; k < matrix.length; k++){
    result.push([]);
  };
  for (var i = 0; i < matrix.length; i ++){
    for (var j = 0; j < matrix.length; j ++){
      result[j].push(matrix[i][j]);
    };
  };
  return result;
};

Assessment.primes = function (n) {
  var nums = [];
  var i = 2;
  while (nums.length < n){
    if (isPrime(i)){
      nums.push(i);
    };
    i++;
  };
  return nums;
};

var isPrime = function (x) {
  for (var i = 2; i < x; i++){
    if (x % i === 0){
      return false;
    };
  };
  return true;
}

Function.prototype.myCall = function (context, args) {
  return this.call(context, args);
};

Function.prototype.inherits = function (Parent) {
  function surrogate() {};
  surrogate.prototype = Parent.prototype;
  this.prototype = new surrogate();

}; // WHAT AM I MISSING