// WakeboardGame module
(function(root)  {
  var WakeboardGame = root.WakeboardGame = (root.WakeboardGame || {});

  WakeboardGame.Boat = function(sponsor) {
    this.sponsor = sponsor;
    this.power = function() {return 'power';};
    this.turn  = function() {return 'turn';};
    this.sink = function() {return 'sink';};

  };

  WakeboardGame.Wakeboarder = function(name, sponsor) {
    this.sponsor = sponsor;
    this.name =  name;
    this.jump = function() {return 'jump';};
    this.spin = function() {return 'spin';};
    this.grind = function() {return 'grind';};
    this.crash = function() {return 'crash';};


  };




})(this);